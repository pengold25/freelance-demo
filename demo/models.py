from django.db import models
from django.contrib.auth.models import User, Group
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver

class Employer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(default='')

class Freelancer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(default='')
    skills = models.TextField(default='')

@receiver(m2m_changed)
def created_group(action, instance, model, **kwargs):
    print('CHANGE GROUP', instance)
    if model == Group:
        if action == 'post_add':
            print('TAO RA USER', instance.groups.all(), hasattr(instance, 'employer'))
            if instance.groups.filter(name='employer').exists() and not hasattr(instance, 'employer'):
                print('TAO EMPLOYER')
                Employer.objects.create(user=instance)
            elif instance.groups.filter(name='freelancer').exists() and not hasattr(instance, 'freelancer'):
                print('TAO FREELANCER')
                Freelancer.objects.create(user=instance)
            pass

class Project(models.Model):
    employer = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.TextField(default='')
    description = models.TextField(default='')