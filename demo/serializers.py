from rest_framework import serializers
from django.contrib.auth.models import User, Group
from .models import Employer, Freelancer, Project

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class FreelancerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Freelancer
        fields = ('url', 'user_id', 'description', 'skills')

class EmployerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Employer
        fields = ('url', 'user_id', 'description')

class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project
        fields = ('url', 'employer', 'name', 'description')
